import React, { useState, useEffect } from 'react';
import { useLocation, useParams} from 'react-router-dom';



const Post = (props) => {

    const [post, setPost] = useState({})
    const { id } = useParams();
    const location = useLocation();
    
    useEffect(()=>{
        //console.log(props.location)
        getPost(id)
    }, {})

    const getPost=(id)=>{
       
        fetch('https://jsonplaceholder.typicode.com/posts/'+id)
        .then(res => {
            return res.json()
        })
        .then(data => {
            //
            
            setPost(data)
            
        })
        .catch(err => {
            console.log('Error ---- ', err)
        })
    }

   

   

    
    return(
        <div className='post'>
            <h1>{post.id}</h1>
            <h3>{post.title}</h3>
            <p>{post.body}</p>
        </div>
        
    )
    

}

export default Post;