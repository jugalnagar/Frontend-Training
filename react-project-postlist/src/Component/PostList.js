import React,{Component} from "react";
import { Link } from "react-router-dom";

class PostList extends Component{

    constructor(props) {
        super(props)
        this.state = {
            posts :[]
        }
    }

    componentDidMount() {
        fetch("https://jsonplaceholder.typicode.com/posts")
        .then(res =>  {
            return res.json();
        }).then( data => {
            this.setState({posts: data});
        })
    }


    render() {
        return (
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>Id</th><th>Title</th><th>Body</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.posts.map(post => (
                            
                            <tr>
                                <Link to={{
                                    pathname: `/post/${post.id}`,
                                    state: post
                                }}><td>{post.id}</td></Link>
                                <td>{post.title}</td>
                                <td>{post.body}</td>
                            </tr>
                            
                            ))
                        }
                    </tbody>
                
                
                </table>
            </div>
        )
    }


}

export default PostList