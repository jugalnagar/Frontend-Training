import logo from './logo.svg';
import { BrowserRouter as Router, Route, Switch,Link, Routes } from 'react-router-dom'
import PostList from './Component/PostList';
import Post from './Component/Post';

import './App.css';

function App() {
  return (
    <Router>

      <div className="App">
        <Link to='/posts'>See Post</Link>
         <p>Click On button to see posts</p>
      </div>



      <Routes>
        <Route exact path="/posts" element={<PostList/>}></Route>
        <Route exact path="/post/:id" element={<Post/>}></Route>
      </Routes>
      
    </Router>
    
  );
}

export default App;
