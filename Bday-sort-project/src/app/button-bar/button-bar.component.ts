import { Component, OnInit, Output,EventEmitter } from '@angular/core';


@Component({
  selector: 'app-button-bar',
  templateUrl: './button-bar.component.html',
  styleUrls: ['./button-bar.component.css']
})
export class ButtonBarComponent implements OnInit {

  @Output() sort = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {

  }

  sortList(basedOn:string) {
    console.log("sort button called");
    this.sort.emit(basedOn);
  }

}
