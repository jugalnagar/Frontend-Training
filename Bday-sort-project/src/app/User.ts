export class User {
    public name: string;
    public birthDate: Date;

    constructor(name:string,birthDate: Date) {
        this.name=name;
        this.birthDate = birthDate;
    }
}