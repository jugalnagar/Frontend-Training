import { Component } from '@angular/core';
import { User } from './User';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  users = [ 
    new User("Jugal",new Date("2015-03-25T12:00:00Z")),
    new User("Dev",new Date("2000-03-25T12:00:00Z")),
    new User("Rahul",new Date("2001-03-25T12:00:00Z")),
    new User("Lekhraj",new Date("1999-03-25T12:00:00Z")),
    new User("Vishnu",new Date("2000-03-27T12:00:00Z")),
    new User("Jitendra",new Date("2001-04-25T12:00:00Z"))
  ]


  title = 'Bday-sort-project';

  sortByName(basedOn:string) {

    console.log("sort of app called");

    if(basedOn.localeCompare("Name")==0) {
      this.users.sort((a,b)=> a.name.localeCompare(b.name));
    }
    else {
      this.users.sort((a,b)=> a.birthDate.getTime()-b.birthDate.getTime());
    }

  
    
  }

}
