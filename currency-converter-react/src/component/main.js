import React, { useEffect, useState } from "react";

const Main = ()=> {
    
    const [inputAmount,setInputAmount] = useState()
    const [outputAmount,setOutputAmount] = useState()
    const [inputCurrency,setInputCurrency] = useState("INR")
    const [outputCurrency,setOutputCurrency] = useState("USD")
    
    const calculate = () => {
        console.log(inputAmount,inputCurrency,outputAmount,outputCurrency)

        if(inputCurrency.localeCompare("INR")==0) {
            if(outputCurrency=="USD"){
                setOutputAmount(inputAmount/74.80)
            }
            else if(outputCurrency=="INR"){
                setOutputAmount(inputAmount)
            }
        }
        else if(inputCurrency.localeCompare("USD")==0) {
            if(outputCurrency=="INR"){
                setOutputAmount(inputAmount*74.80)
            }
            else if(outputCurrency=="USD"){
                setOutputAmount(inputAmount)
            }
        }
        
    }

    useEffect(()=>{calculate()},[inputCurrency,inputAmount,outputCurrency])
  
   function changeInputAmount(e){
       console.log("ia changed")
        setInputAmount(e.target.value)
        //calculate();
    }
    function changeInputCurrency(e){
        console.log("ic changed")
        setInputCurrency(e.target.value)
        //calculate();
    }
    function changeOutputCurrency(e){
        console.log("oc changed")
        setOutputCurrency(e.target.value)
        //calculate();
    }

    return (
        <div className="container">
            <h3>Currency Converter</h3>
            <div className="inputContainer">
                From <input type="number" value={inputAmount} onChange={changeInputAmount}/>
                <select onChange={changeInputCurrency}>
                    <option value={"INR"}>INR</option>
                    <option value={"USD"}>USD</option>
                    <option value={"INR"}>INR</option>
                </select>
            </div>
            <div className="outputContainer">
                To <input type="number" value={outputAmount} readOnly/>
                    <select onChange={changeOutputCurrency}>
                        <option value={"USD"}>USD</option>
                        <option value={"INR"}>INR</option>
                        <option value={"INR"}>INR</option>
                    </select>
            </div>
            
        </div>
    );
}

export default Main;